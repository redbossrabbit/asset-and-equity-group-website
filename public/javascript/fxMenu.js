const ham = document.getElementById('ham');
const dropDown = document.getElementById('drop-down');
const black = document.getElementById('black');
ham.addEventListener('click', () => {
    DROP();
});
black.addEventListener('click', () => {
    HIDE();
});
const DROP = () => {
    if (ham.classList.contains('is-open')) {
        HIDE();
    } else {
        dropDown.style.display = 'flex';
        black.style.display = 'flex';
        ham.classList.toggle('is-open');
    }
}
const HIDE = () => {
    dropDown.style.display = 'none';
    black.style.display = 'none';
    ham.classList.toggle('is-open');
}